﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneController : MonoBehaviour {

    public GameObject image;
    public GameObject DrawUI;
    public GameObject MenuUI;
    public GameObject ColorPickerUI;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ImageSelected(GameObject selectedImage)
    {
        image = selectedImage;

        GameObject.Destroy(GameObject.FindGameObjectWithTag("MenuUI"));

        image = GameObject.Instantiate(selectedImage, GameObject.Find("Canvas").transform,false);

        GameObject.Instantiate(DrawUI, GameObject.Find("Canvas").transform, false);

    }

    public void MenuSelected()
    {
        GameObject.Destroy(GameObject.FindGameObjectWithTag("Image"));
        GameObject.Destroy(GameObject.FindGameObjectWithTag("DrawingUI"));

        GameObject.Instantiate(MenuUI,GameObject.Find("Canvas").transform,false);
    }

    public void ColorPickerSelected(bool Selected)
    {
        if (Selected)
        {
            GameObject.Instantiate(ColorPickerUI);
            image.GetComponent<DrawController>().colorPicker = true;
        }
        else
        {
            image.GetComponent<DrawController>().drawColor = GameObject.FindGameObjectWithTag("ColorPicker").GetComponent<Image>().color;
            image.GetComponent<DrawController>().colorPicker = false;
            Destroy(GameObject.FindGameObjectWithTag("ColorPickerCanvas"));
            
        }
    }
}
