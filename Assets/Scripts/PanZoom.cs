﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanZoom : MonoBehaviour {
    public bool move;
    Vector3 mousePos;
    int width = 600;
    int height = 600;
    RawImage canvasRaw;

    Vector2 startPos;
    // Use this for initialization
    void Start ()
    {
        canvasRaw = this.GetComponent<RawImage>();
	}
	
	// Update is called once per frame
	void Update ()
    {

        Zoom(Input.GetAxis("Mouse ScrollWheel"));
        if (move)
        {
            if(Input.touchCount == 1)
            {
                Vector2 direction = Vector2.zero;
                Touch touch = Input.GetTouch(0);
                if(touch.phase == TouchPhase.Began)
                {
                    startPos = touch.position;
                }
                else if(touch.phase == TouchPhase.Moved)
                {
                    direction = (touch.position - startPos);
                    startPos = touch.position;
                    Debug.Log("moved" + direction);
                }
                Pan(direction * new Vector2(0.0005f,0.0005f));
            }
        }
        if (Input.touchCount == 2)
        {
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            Vector2 touchZeroPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePos = touchOne.position - touchOne.deltaPosition;

            //Vector2 touchZeroMag = (touchZero.position - touchZeroPos);
            //Vector2 touchOneMag = (touchOne.position - touchOnePos);

            //Vector2 touchDelta = (touchZeroMag * touchOneMag) / 2;
            
            float prevMagnitude = (touchZeroPos - touchOnePos).magnitude;
            float currentMagnitude = (touchZero.position - touchOne.position).magnitude;

            float difference = currentMagnitude - prevMagnitude;

            Zoom(difference * 0.001f);
            //Pan(touchDelta);
        }	
	}

    private void Pan(Vector2 pan)
    {
        if ((((canvasRaw.uvRect.x - pan.x) + canvasRaw.uvRect.width < 1) && ((canvasRaw.uvRect.x - pan.x) > 0)) && ((canvasRaw.uvRect.y - pan.y) + canvasRaw.uvRect.height < 1) && ((canvasRaw.uvRect.y - pan.y) > 0))
        {
            canvasRaw.uvRect = new Rect((canvasRaw.uvRect.x - pan.x), (canvasRaw.uvRect.y - pan.y), canvasRaw.uvRect.width, canvasRaw.uvRect.height);
        }
    }
    private void Zoom(float zoomAmount)
    {
        Rect change = new Rect();
        if(canvasRaw.uvRect.width - zoomAmount + canvasRaw.uvRect.x > 1)
        {
            change.x = (1 - canvasRaw.uvRect.width);
        }
        if (canvasRaw.uvRect.height - zoomAmount + canvasRaw.uvRect.y > 1)
        {
            change.y = (1 - canvasRaw.uvRect.height);
        }
        else if(canvasRaw.uvRect.height - zoomAmount + canvasRaw.uvRect.y < 1 && canvasRaw.uvRect.width - zoomAmount + canvasRaw.uvRect.x < 1)
        {
            change.x = canvasRaw.uvRect.x;
            change.y = canvasRaw.uvRect.y;
        }
        change.width = Mathf.Clamp(canvasRaw.uvRect.width - zoomAmount,0,1);
        change.height = Mathf.Clamp(canvasRaw.uvRect.height - zoomAmount,0,1);

        canvasRaw.uvRect = change;
        
    }


    public void SetMove(bool value)
    {
        move = value;
    }
}
