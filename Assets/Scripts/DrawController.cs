﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DrawController : MonoBehaviour {

    public bool move = false;
    public bool erase = false;
    public bool colorPicker;
    public int drawSize=20;

    Texture2D canvasTexture;
    public Texture2D clear;

    public Color drawColor = Color.red;
    RawImage canvasRaw;

    public int width;
    public int height;

    //mouse
    Vector2 mousePos = new Vector2();
    RectTransform rectTransform;

	// Use this for initialization
	void Start () {
        canvasRaw = GetComponent<RawImage>();
        canvasTexture = canvasRaw.texture as Texture2D;

        rectTransform = canvasRaw.GetComponent<RectTransform>();
        width = canvasTexture.width;
        height = canvasTexture.height;
    }
	
	// Update is called once per frame
	void Update () {
        if (move)
        {
            return;
        }
        if (Input.touchCount == 1)
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, Input.GetTouch(0).position, Camera.main, out mousePos);
            if (mousePos.y < -300)
            {
                return;
            }

            mousePos.x = width - (width / 2 - mousePos.x);
            mousePos.y = Mathf.Abs((height / 2 - mousePos.y) - height);
            int PixelAmount = (int)(width * canvasRaw.uvRect.width);
            int startPixelX = (int)((width * canvasRaw.uvRect.x));
            int startPixelY = (int)((width * canvasRaw.uvRect.y));
            double pixelPercentageY = mousePos.y / height;
            double pixelPercentageX = mousePos.x / width;
            mousePos.x = (float)(startPixelX + PixelAmount * pixelPercentageX);
            mousePos.y = (float)(startPixelY + PixelAmount * pixelPercentageY);

        if (colorPicker)
        {
            GameObject.FindGameObjectWithTag("ColorPicker").GetComponent<Image>().color = canvasTexture.GetPixel((int)mousePos.x, (int)mousePos.y);
        }
        else
        {

            if (erase)
            {
                for (int i = -(drawSize); i < drawSize; i++)
                {
                    for (int j = -(drawSize); j < drawSize; j++)
                    {
                        canvasTexture.SetPixel((int)mousePos.x + i, (int)mousePos.y + j, new Color(clear.GetPixel((int)mousePos.x + i, (int)mousePos.y + j).r, clear.GetPixel((int)mousePos.x + i, (int)mousePos.y + j).g, clear.GetPixel((int)mousePos.x + i, (int)mousePos.y + j).b));
                    }
                }
            }
            else if (!erase)
            {
                for (int i = -(drawSize); i < drawSize; i++)
                {
                    for (int j = -(drawSize); j < drawSize; j++)
                    {
                        if ((clear.GetPixel((int)mousePos.x + i, (int)mousePos.y + j).r + clear.GetPixel((int)mousePos.x + i, (int)mousePos.y + j).g + clear.GetPixel((int)mousePos.x + i, (int)mousePos.y + j).b) < 2)
                        {
                            canvasTexture.SetPixel((int)mousePos.x + i, (int)mousePos.y + j, Color.black);
                        }
                        else
                        {
                            canvasTexture.SetPixel((int)mousePos.x + i, (int)mousePos.y + j, new Color(drawColor.r, drawColor.g, drawColor.b));
                        }
                    }
                }
            }
        }
        }
        else
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, Input.mousePosition, Camera.main, out mousePos);
            if (mousePos.y < -300)
            {
                return;
            }
            mousePos.x = width - (width / 2 - mousePos.x);
            mousePos.y = Mathf.Abs((height / 2 - mousePos.y) - height);

            int PixelAmount = (int)(width * canvasRaw.uvRect.width);
            int startPixelX = (int)((width * canvasRaw.uvRect.x));
            int startPixelY = (int)((width * canvasRaw.uvRect.y));
            double pixelPercentageY = mousePos.y / height;
            double pixelPercentageX = mousePos.x / width;

            mousePos.x = (float)(startPixelX + PixelAmount * pixelPercentageX);
            mousePos.y = (float)(startPixelY + PixelAmount * pixelPercentageY);


            //if (Input.GetMouseButton(0))
            //{
            //    if (mousePos.x >= 0 && mousePos.x <= width && mousePos.y >= 0 && mousePos.y <= height)
            //    {
            //        if (colorPicker)
            //        {
            //            GameObject.FindGameObjectWithTag("ColorPicker").GetComponent<Image>().color = canvasTexture.GetPixel((int)mousePos.x, (int)mousePos.y);
            //        }
            //        else
            //        {
            //            if (erase)
            //            {
            //                for (int i = -(drawSize); i < drawSize; i++)
            //                {
            //                    for (int j = -(drawSize); j < drawSize; j++)
            //                    {
            //                        canvasTexture.SetPixel((int)mousePos.x + i, (int)mousePos.y + j, new Color(clear.GetPixel((int)mousePos.x + i, (int)mousePos.y + j).r, clear.GetPixel((int)mousePos.x + i, (int)mousePos.y + j).g, clear.GetPixel((int)mousePos.x + i, (int)mousePos.y + j).b));
            //                    }
            //                }
            //            }
            //            else if (!erase)
            //            {
            //                for (int i = -(drawSize); i < drawSize; i++)
            //                {
            //                    for (int j = -(drawSize); j < drawSize; j++)
            //                    {
            //                        if ((clear.GetPixel((int)mousePos.x + i, (int)mousePos.y + j).r + clear.GetPixel((int)mousePos.x + i, (int)mousePos.y + j).g + clear.GetPixel((int)mousePos.x + i, (int)mousePos.y + j).b) < 2)
            //                        {
            //                            canvasTexture.SetPixel((int)mousePos.x + i, (int)mousePos.y + j, Color.black);
            //                        }
            //                        else
            //                        {
            //                            canvasTexture.SetPixel((int)mousePos.x + i, (int)mousePos.y + j, new Color(drawColor.r, drawColor.g, drawColor.b));
            //                        }

            //                    }
            //                }
            //            }
            //        }
            //    }
            //}
        }
        canvasTexture.Apply();
	}

    public void Clear()
    {
        canvasTexture.SetPixels(clear.GetPixels());
        canvasTexture.Apply();
    }

    public void SetDrawSize(float size)
    {
        drawSize = (int)size;
    }

    public void SetEraser(bool set)
    {
        erase = set;
    }

    public void SetMove(bool value)
    {
        move = value;
    }
}
