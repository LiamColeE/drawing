﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorToggleController : MonoBehaviour
{
    public Toggle toggle;
    private SceneController SC;

    // Use this for initialization
    void Start()
    {
        SC = GameObject.Find("SceneScripts").GetComponent<SceneController>();
        toggle = this.gameObject.GetComponent<Toggle>();
        toggle.onValueChanged.AddListener((value) => { ValueChanged(); }   
       );   
    }
    private void OnDestroy()
    {
        //toggle.onValueChanged.RemoveListener(() => Click());
    }

    void ValueChanged()
    {
        SC.ColorPickerSelected(toggle.isOn);
    }
}
