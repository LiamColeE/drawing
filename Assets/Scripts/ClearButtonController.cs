﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClearButtonController : MonoBehaviour {

    private Button button;
    private GameObject image;

	// Use this for initialization
	void Start () {
        button = this.gameObject.GetComponent<Button>();
        button.onClick.AddListener(() => Click());

        image = GameObject.FindGameObjectWithTag("Image");
    }
    private void OnDestroy()
    {
        button.onClick.RemoveListener(() => Click());
    }

    void Click()
    {
        image.GetComponent<DrawController>().Clear();
    }
}
