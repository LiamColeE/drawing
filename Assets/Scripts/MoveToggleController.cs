﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveToggleController : MonoBehaviour {

    private GameObject image;
    private Toggle toggle;
    private GameObject eraserToggle;
    private GameObject text;

    // Use this for initialization
    void Start()
    {
        eraserToggle = GameObject.Find("Eraser");
        image = GameObject.FindGameObjectWithTag("Image");
        toggle = this.gameObject.GetComponent<Toggle>();
        text = this.transform.GetChild(1).gameObject;
        toggle.onValueChanged.AddListener((value) => { Click(value); });
    }

    // Update is called once per frame
    void Update()
    {
        //Click(toggle.isOn);
    }

    void Click(bool value)
    {
        image.GetComponent<DrawController>().SetMove(value);
        image.GetComponent<PanZoom>().SetMove(value);

        if (value == true)
        {
            text.GetComponent<Text>().text = "Draw";
        }
        else
        {
            text.GetComponent<Text>().text = "Move";
            eraserToggle.GetComponent<Toggle>().isOn = value;
        }
    }

    public void setToggle(bool value)
    {
        toggle.isOn = value;
    }
}
