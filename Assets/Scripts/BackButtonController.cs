﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackButtonController : MonoBehaviour {

    private Button button;
    private GameObject sceneController;

    // Use this for initialization
    void Start()
    {
        button = this.gameObject.GetComponent<Button>();
        button.onClick.AddListener(() => Click());

        sceneController = GameObject.Find("SceneScripts");
    }
    private void OnDestroy()
    {
        button.onClick.RemoveListener(() => Click());
    }

    void Click()
    {
        sceneController.GetComponent<SceneController>().MenuSelected();
    }
}
