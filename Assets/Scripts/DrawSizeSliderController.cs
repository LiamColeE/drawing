﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DrawSizeSliderController : MonoBehaviour {

    private GameObject image;
    private Slider slider;

	// Use this for initialization
	void Start () {
        image = GameObject.FindGameObjectWithTag("Image");
        slider = this.GetComponent<Slider>();
	}
	
	// Update is called once per frame
	void Update () {
        Slide(slider.value);
	}

    void Slide(float size)
    {
        image.GetComponent<DrawController>().SetDrawSize(size);
    }
}
