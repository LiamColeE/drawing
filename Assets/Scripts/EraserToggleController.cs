﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class EraserToggleController : MonoBehaviour {

    private GameObject image;
    private Toggle toggle;
    private GameObject text;
    private GameObject moveToggle;

	// Use this for initialization
	void Start () {
        moveToggle = GameObject.Find("Move");
        image = GameObject.FindGameObjectWithTag("Image");
        toggle = this.gameObject.GetComponent<Toggle>();
        text = this.transform.GetChild(1).gameObject;
        toggle.onValueChanged.AddListener((value) => { Click(value); });
    }
	
	// Update is called once per frame
	void Update () {
        //Click(toggle.isOn);
	}

    void Click(bool value)
    {
        image.GetComponent<DrawController>().SetEraser(value);
        if(value == true)
        {
            text.GetComponent<Text>().text = "Pencil";
        }
        else
        {
            text.GetComponent<Text>().text = "Eraser";
            moveToggle.GetComponent<Toggle>().isOn = value;
        }
    }

    public void setToggle(bool value)
    {
        toggle.isOn = value;
    }
}
